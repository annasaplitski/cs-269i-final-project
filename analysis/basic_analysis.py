import database_queries as db
import math

TABLE = "`general`"

INDEP_MAYOR = 'WALIRAHMANMAYOR'
REP_MAYOR = 'KARENBROWNMAYOR'
DEM_MAYOR = 'MICHAELANUTTERMAYOR'

REP_CC_INCUMBENT = 'JOSEPHJDUDACITYCOMMISSIONERS'
REP_CC_REFORMER = 'ALSCHMIDTCITYCOMMISSIONERS'
DEM_CC_INCUMBENT = 'ANTHONYCLARKCITYCOMMISSIONERS'
DEM_CC_REFORMER= 'STEPHANIESINGERCITYCOMMISSIONERS'

GREEN_SHERIFF = 'CHERILHONKALASHERIFF'
REP_SHERIFF = 'JOSHUARWESTSHERIFF'
DEM_SHERIFF = 'JEWELLWILLIAMSSHERIFF'

COLUMNS = [INDEP_MAYOR, REP_MAYOR, DEM_MAYOR,
           REP_CC_INCUMBENT, REP_CC_REFORMER,
           DEM_CC_INCUMBENT, DEM_CC_REFORMER,
           GREEN_SHERIFF, REP_SHERIFF, DEM_SHERIFF]

def main():
    major_vote_totals = db.aggregate_sums_of_columns_by_position(
        TABLE, "MAYOR")
    total_voters = sum(major_vote_totals.values())
    print(total_voters)
    print(major_vote_totals)
    comm_totals=db.aggregate_sums_of_columns_by_position(
        TABLE, "CITYCOMMISSIONERS")
    print(comm_totals)
    print(sum(comm_totals.values())/2)
    sheriff_vote_totals = db.aggregate_sums_of_columns_by_position(
        TABLE, "SHERIFF")
    print(sheriff_vote_totals)
    print(sum(sheriff_vote_totals.values()))

    col_to_index = dict([(col, i) for i,col in enumerate(COLUMNS)])

    [names, rows] = db.sums_columns_by_position(
        TABLE, ['MAYOR', 'SHERIFF', 'CITYCOMMISSIONERS'])
    print(names)
    mismatch = 0
    for row in rows:
        row = (row[0], row[1], row[2]/2)
        if row[2] > row[1]:
            mismatch += 1
        #print(row)
    print(mismatch)

    [names, rows] = db.select_columns(TABLE, COLUMNS)
    print(names)
    for i in range(5):
        print(rows[i])
    
    rows = db.execute_query(
        '''SELECT ({} - {} - {}),
        ({} - {} - {}),
        ({}- {}),
        ({} - {})
        FROM
        {}'''.format(DEM_CC_REFORMER, DEM_SHERIFF, GREEN_SHERIFF,
                     REP_CC_REFORMER, REP_SHERIFF, GREEN_SHERIFF,
                     DEM_CC_INCUMBENT, DEM_SHERIFF,
                     REP_CC_INCUMBENT, REP_SHERIFF,
                     TABLE))
    for i in range(5):
        print(rows[i])

    l2_norm = 0
    for row in rows:
        l2_norm += sum(row)*sum(row)
    print(math.sqrt(l2_norm))


if __name__ == "__main__":
    main()
