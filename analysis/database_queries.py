import sqlite3 as sqlite

DATABASE = '../data/project.db'
TABLE_NAMES = ['`general`', '`primary`']

def list_columns(table):
    '''Takes a string table name and returns the list of
    columns in the table.'''
    conn = sqlite.connect(DATABASE)
    c = conn.cursor()
    c.execute('PRAGMA table_info(%s)' % table)
    column_names = [row[1] for row in c.fetchall()]
    conn.commit()
    conn.close()
    return column_names


def execute_query(query):
    conn = sqlite.connect(DATABASE)
    c = conn.cursor()
    c.execute(query)
    rows = c.fetchall()
    conn.commit()
    conn.close()
    return rows # a list of tuples
    

def select_columns(table, names):
    '''Returns un-aggregated data with only certain columns,
    i.e. candidates, highlighted. The list of names gives the
    desired columns to be selected.'''
    query = 'SELECT '
    for i in range(len(names)-1):
        query += '{}, '.format(names[i])
    query += '{} '.format(names[-1])
    query += 'FROM {}'.format(table)
    return (names,execute_query(query))


def select_columns_by_number(table, nums):
    '''Allows you to select columns by column number.'''
    columns = list_columns(table)
    return select_columns(table, [columns[i] for i in nums])


def select_columns_by_position(table, position):
    '''Takes a position, e.g. "MAYOR", and lists all related
    columns.'''
    columns = list_columns(table)
    return select_columns(
        table, [col for col in columns if position in col])


def aggregate_sums_of_columns(table, names):
    '''Returns a map column --> count.'''
    query = 'SELECT '
    for i in range(len(names)-1):
        query += 'SUM({}), '.format(names[i])
    query += 'SUM({}) '.format(names[-1])
    query += 'FROM {}'.format(table)
    row = execute_query(query)[0]
    aggregate_sums = {}
    for i in range(len(names)):
        aggregate_sums[names[i]] = row[i]
    return aggregate_sums


def aggregate_sums_of_columns_by_number(table, nums):
    '''Allows you to select columns by column number.'''
    columns = list_columns(table)
    return aggregate_sums_of_columns(table, [columns[i] for i in nums])


def aggregate_sums_of_columns_by_position(table, position):
    '''Takes a position, e.g. "MAYOR", and lists all related
    columns.'''
    columns = list_columns(table)
    return aggregate_sums_of_columns(
        table, [col for col in columns if position in col])


def sums_columns_by_position(table, positions):
    columns = list_columns(table)
    new_columns = []
    for position in positions:
        position_cols = [col for col in columns if position in col]
        new_column = '+'.join(position_cols) + ' AS ' + position
        new_columns.append(new_column)
    return select_columns(table, new_columns)


def main():
    columns = list_columns(TABLE_NAMES[0])
    print(columns)
    print(aggregate_sums_of_columns_by_position(TABLE_NAMES[0], 'MAYOR'))

if __name__ == "__main__":
    main()

