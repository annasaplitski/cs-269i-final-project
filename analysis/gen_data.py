# This will be some functions to generate random voter preference data and analyze election results.

import random
import math
import matplotlib.pyplot as plt
import numpy as np
import pylab
import math
from scipy import stats, optimize

n = 3 # number of candidates who will be elected
k = 2 # number of possible votes per person
c = 4 # number of candidates in the race
v = 1000 # number of voters

# for each demographic, the proportion of the population it is
# and then a list of multipliers per candidate
# first 2 candidates = democrat; second 2 = republican (first republican gets cross-party votes)
# est. dem., singer, schmidt, est. rep
demographic_multipliers = ([
    (0.60, [0.60, 0.25, 0.05, 0.10]), # establishment democrats 110 -> 60
    (0.15, [0.10, 0.05, 0.25, 0.60]), # establishment republicans 26 -> 15
    (0.20, [0.25, 0.60, 0.10, 0.05]), # reform democrats
    (0.05, [0.05, 0.10, 0.60, 0.25]) # reform republicans
])

utility_multipliers = ([
    (0.60, [0.60, 0.25, 0.05, 0.10]), # establishment democrats 110 -> 60
    (0.15, [0.10, 0.05, 0.25, 0.60]), # establishment republicans 26 -> 15
    (0.20, [0.25, 0.60, 0.10, 0.05]), # reform democrats
    (0.05, [0.05, 0.10, 0.60, 0.25]) # reform republicans
])

def is_silly(index, silly_pop):
	# silly_pop = proportion of reps who are silly
	# if index > 0.6*v and index < 0.6*v + 0.15*v*silly_pop:
	# 	return True
	if index > 0.95*v and index < 0.95*v + 0.05*v*silly_pop:
		return True
	return False


def check_multipliers():
	# I couldn't come up with something better than hardcoding those, so this just checks
	# array lengths etc.
	assert len(demographic_multipliers) > 0
	voter_pop = 0
	for entry in demographic_multipliers:
		voter_pop += entry[0]
		assert len(entry[1]) == c
		# assert sum(entry[1]) == 1 # doesn't work for all float sums
	assert voter_pop == 1 # proportions of population must sum to 1

def get_candidate_choice(rand, demographic_info, rem_candidates):
	# Given the demographic info for this voter and the list of candidates remaining, 
	# calculates relative probabilities of voting for different remaining candidates and 
	# maps the 0-1 float space to candidate choices. Given the random value rand from [0.1),
	# returns the corresponding candidate number (which must be in rem_candidates).

	total_prob_remaining = 0 # normalizing factor
	for candidate in rem_candidates:
		total_prob_remaining += demographic_info[candidate] # candidate #s are indices too!

	# now actually pick the right candidate
	prob_so_far = 0
	for candidate in rem_candidates:
		prob_so_far += demographic_info[candidate]/total_prob_remaining
		if rand < prob_so_far:
			return candidate
	# we should never get here; at the end prob_so_far should be 1
	raise "oh no!"

	# For right now, just spread uniformly across all the candidates.
	# index = int(math.floor(rand*len(rem_candidates)))
	# return rem_candidates[index]

def gen_preferences(demographic_info):
	# Generates a ranked order of candidates for a single voter (i.e. one preference list.)
	# Currently, assumes equal preference weights throughout the ranked list, so doesn't include those.
	preferences = []
	rem_candidates = range(c) # candidates remaining for selection
	for i in xrange(c):
		# pick a new candidate to prefer, from the list of remaining candidates
		cur_choice = get_candidate_choice(random.random(), demographic_info, rem_candidates)
		preferences.append(cur_choice)
		rem_candidates.remove(cur_choice)
	return preferences

def gen_all_preferences():
	# generate a preference list for every voter, returned in a 2D list.
	check_multipliers()

	all_preferences = []
	curr_demographic_index = 0
	proportion_sum = demographic_multipliers[0][0]
	for i in xrange(v):
		# pick the demographic for this voter based on # of voters so far
		if i/(float(v)) >= proportion_sum:
			# move to next demographic
			curr_demographic_index += 1
			proportion_sum += demographic_multipliers[curr_demographic_index][0]

		all_preferences.append(gen_preferences(
			demographic_multipliers[curr_demographic_index][1]))

	return all_preferences

# ------------------------------------------------------------ #
# Above is preference generation; below is election result analysis.
# ------------------------------------------------------------ #

def tally_honest_votes(all_preferences, silly_pop=0):
	# return list of winning candidates with their respective #s of votes
	# return format: [(candidate, votes), (candidate, votes)]
	votes = {}
	for index in xrange(len(all_preferences)):
		preference = all_preferences[index]
		num_votes = k
		if is_silly(index, silly_pop):
			num_votes = k-1
		for i in xrange(num_votes):
			candidate = preference[i]
			if candidate not in votes:
				votes[candidate] = 1
			else:
				votes[candidate] += 1

	# return top n candidates
	to_return = [(candidate, votes[candidate]) for candidate in sorted(votes, key=votes.get, reverse=True)]
	return to_return

# ------------------------------------------------------------ #
# Above is election result analysis methods; below is what's actually run.
# ------------------------------------------------------------ #
num_reps = 500
wins = {} # total across all trials
votes = {} # total across all trials
votes_avg = {} # avg across all trials
utility_avgs = {} # avg across all trials
def repeated_trials(silly_pop=0):
	# fills in wins, votes, and utility_avgs
	# wins = {} # total across all trials
	# votes = {} # total across all trials
	# votes_avg = {} # avg across all trials
	# utility_avgs = {} # avg across all trials
	for i in xrange(num_reps):
		all_preferences = gen_all_preferences()
		results = tally_honest_votes(all_preferences, silly_pop=silly_pop)
		winners = results[:n]

		for dem_index in xrange(len(utility_multipliers)):
			demographic = utility_multipliers[dem_index]
			utility = sum(demographic[1][winner[0]] for winner in winners)/num_reps
			if dem_index not in utility_avgs:
				utility_avgs[dem_index] = utility
			else:
				utility_avgs[dem_index] += utility

		for result in results:
			if result[0] not in votes:
				votes[result[0]] = result[1]
			else: 
				votes[result[0]] += result[1]

			if result in winners:
				if result[0] not in wins:
					wins[result[0]] = 1
				else:
					wins[result[0]] += 1

	for candidate in votes:
		votes_avg[candidate] = votes[candidate]/float(num_reps)

	# for now, just return wins
	return wins

# output error catching for unpopular candidates
# for i in xrange(c):
# 	if c not in wins:
# 		wins[c] = 0
# 	if c not in votes:
# 		votes[c] = 0
	# # print all_preferences
	# print "with honest voting, winners are: "
	# print tally_honest_votes(all_preferences)
def gather_utilities():
	utilities = [[],[],[],[]]
	weight_transfered = []
	# demographic_multipliers[2][1][2] = 0.10
	# demographic_multipliers[2][1][0] = 0.25
	for i in xrange(100):
		print 'new round'
		print demographic_multipliers
		utility_avgs = repeated_trials(silly_pop=i)
		print utility_avgs
		for index in xrange(len(utilities)):
			if index not in utility_avgs:
				utilities[index].append(0)
			else:
				utilities[index].append(utility_avgs[index])
		weight_transfered.append(i)
		# demographic_multipliers[2][1][2] += 0.01
		# demographic_multipliers[2][1][0] -= 0.01
		wins = {} # total across all trials
		votes = {} # total across all trials
		votes_avg = {} # avg across all trials
		utility_avgs = {} # avg across all trials
	return (utilities, weight_transfered)

# utilities, weight_transfered = gather_utilities()

# # make truthful line
# baseline_y_2 = [0.9 for x in weight_transfered_2]
# baseline_y_3 = [0.4 for x in weight_transfered_2]

# weight_transfered = range(100)
# win = [500 for x in weight_transfered]
# lose = [0 for x in weight_transfered]


# plot the samples
# fig = plt.figure()
# ax = plt.gca()
# # ax.plot(weight_transfered, utilities[0], c='blue', label='Dem. Establishment wins')
# # ax.plot(weight_transfered, utilities[1], c='green', label='Dem. Reform wins')
# ax.plot(weight_transfered, utilities[2], c='green', label='Rep. Reform wins')
# ax.plot(weight_transfered, utilities[3], c='black', label='Dem./Rep. Establishment wins')
# ax.legend()
# fig.suptitle('Effect of casting <k votes on wins', fontsize=20)
# plt.xlabel('Proportion of Republicans casting only 1 vote', fontsize=18)
# plt.ylabel('Number of wins across 500 runs', fontsize=16)
# pylab.show()

repeated_trials()
print "win totals are: "
print wins
print "average votes per candidate are: "
print votes_avg
# print "republican wins are %d, democrate are %d, ratio %f" % (wins[2] + wins[3],
# 	wins[0] + wins[1], (wins[2]+wins[3])/float(wins[0]+wins[1]))
print "average utility for each demographic:"
print utility_avgs


